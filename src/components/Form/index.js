import { useState } from "react";
import "./style.css";

function Form({ adicionar }) {
  const [tarefa, setTarefa] = useState("");
  //   console.log("tarefa: ", tarefa);

  function atualizaEntrada() {
    if (tarefa !== "") {
      adicionar(tarefa);
      setTarefa("");
    }
  }
  return (
    <div className="container_entrada">
      <div className="container_input">
        <input
          type="text"
          placeholder="Nova Tarefa"
          value={tarefa}
          onChange={(e) => setTarefa(e.target.value)}
          required
        ></input>
      </div>

      <div className="container_button">
        <button onClick={atualizaEntrada} className="botaoDefault">
          Enviar
        </button>
      </div>
    </div>
  );
}
export default Form;
