import "./style.css";
function TodoList({ remover, todo }) {
  return (
    <div className="container_todolist">
      <ul className="lista">
        {todo.map((elem, index) => (
          <li key={index} className="lista-elemento">
            <span>{elem}</span>
            <button className="botaoDefault" onClick={() => remover(elem)}>
              Concluir Tarefa
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
}
export default TodoList;
