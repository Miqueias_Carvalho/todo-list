import { useState } from "react";
import "./App.css";
import Form from "./components/Form";
import TodoList from "./components/TodoList";

function App() {
  const [todo, setTodo] = useState([]);

  console.log("todo", todo);

  const addTodo = (newTodo) => {
    setTodo([...todo, newTodo]);
  };

  const handleTodo = (removeThisItem) => {
    // console.log(removeThisItem);
    let listaResultante = todo.filter((item) => {
      return item !== removeThisItem;
    });
    setTodo(listaResultante);
  };

  return (
    <div className="App">
      <div className="App-header">
        <Form adicionar={addTodo} />
        <TodoList todo={todo} remover={handleTodo} />
      </div>
    </div>
  );
}

export default App;
